import React from 'react';
import { Card, Button } from 'react-bootstrap';

function Cards() {
  return (
   <div>
    <Card style={{ width: '18rem' }}>
    <Card.Body>
        <Card.Title>Judul Kegiatan</Card.Title>
        <Card.Text>
        Deskripsi kegiatan misalnya : mandi, makan, berangkat, naik angkot, kerja, istirahat, kerja lagi, pulang :))).
        </Card.Text>
        <Button variant="primary">Udah blom?</Button>
    </Card.Body>
    </Card>
  </div>
  );
}

export default Cards;
