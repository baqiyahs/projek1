import React from 'react';
import { Link } from 'react-router-dom';

function SideBar(){
	return(
		<div id="sidebar-menu">
			<Link to="/">Home</Link>
			<Link to="/add">Add</Link>
		</div>
		);
}

export default SideBar;