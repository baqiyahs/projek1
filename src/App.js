import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Card from './Component/Card';
import Home from './View/Home';
import Add from './View/Add';
import SideBar from './Component/SideBar';

function App() {
  return (
    <Router>
      <div>
      <SideBar/>
        <div className="container py-4">
          <Switch>
            <Route path='/add'>
              <Add/>
            </Route>
            <Route path='/'>
              <Home/>
            </Route>
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
